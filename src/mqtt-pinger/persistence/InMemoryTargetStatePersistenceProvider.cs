﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace mqtt_pinger.persistence
{
    public class InMemoryTargetStatePersistenceProvider:ITargetStatePersistenceProvider
    {
        private readonly IDictionary<string, bool> _state = new Dictionary<string, bool>();

        public Task<bool?> GetStateAsync(string ip)
        {
            return Task.FromResult(_state.TryGetValue(ip, out var val)? val:(bool?)null);
        }
        public Task StoreStateAsync(string ip, bool isOkPing)
        {
            if (_state.ContainsKey(ip))
            {
                _state[ip] = isOkPing;
            }
            else
            {
                _state.Add(ip, isOkPing);
            }

            return Task.CompletedTask;
        }
    }
}
