﻿using System.Threading.Tasks;

namespace mqtt_pinger.persistence
{
    public interface ITargetStatePersistenceProvider
    {
        Task<bool?> GetStateAsync(string ip);
        Task StoreStateAsync(string ip, bool isOkPing);
    }
}
