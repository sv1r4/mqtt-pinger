﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using mqtt_pinger.config;
using mqtt_pinger.extensions;
using mqtt_pinger.persistence;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;
using Serilog;

namespace mqtt_pinger
{
    class Program
    {
        static async Task Main()
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration(ConfigureConfiguration)
                .ConfigureLogging(ConfigureLogging)
                .ConfigureServices(ConfigureServices)
                .UseConsoleLifetime();

            await host.Build().RunAsync();
        }

        private static void ConfigureLogging(HostBuilderContext context, ILoggingBuilder logging)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Application", LoggingExtensions.ApplicationName.Get)
                .WriteTo.Console(
                    //new RenderedCompactJsonFormatter() - renable json formatter after seq or other service integration
                    )
                .ReadFromMicrosoftConfiguration(context.Configuration)
                .CreateLogger();

            logging.AddSerilog(dispose:true);
        }

        private static void ConfigureConfiguration(IConfigurationBuilder config)
        {
            config
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
        }

        private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<MqttConfig>(context.Configuration.GetSection(nameof(MqttConfig)));
            services.Configure<PingConfig>(context.Configuration.GetSection(nameof(PingConfig)));

            services.AddSingleton<IManagedMqttClient>(new MqttFactory().CreateManagedMqttClient());
            services.AddHostedService<MqttPinger>();

            services.AddSingleton<ITargetStatePersistenceProvider, InMemoryTargetStatePersistenceProvider>();
        }



    }
}
