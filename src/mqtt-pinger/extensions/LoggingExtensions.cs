﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace mqtt_pinger.extensions
{
    public static class LoggingExtensions
    { 
        private static readonly IDictionary<LogLevel, LogEventLevel> MsToSerilogLevelMap =
            new Dictionary<LogLevel, LogEventLevel>
            {
                {LogLevel.Trace, LogEventLevel.Verbose},
                {LogLevel.Debug, LogEventLevel.Debug},
                {LogLevel.Information, LogEventLevel.Information},
                {LogLevel.Warning, LogEventLevel.Warning},
                {LogLevel.Error, LogEventLevel.Error},
                {LogLevel.Critical, LogEventLevel.Fatal}
            };

        public static LoggerConfiguration ReadFromMicrosoftConfiguration(this LoggerConfiguration loggerConfiguration,
            IConfiguration configuration)
        {
            var loggingSection = configuration.GetSection("Logging");
            if (loggingSection?.Exists()!=true) {return loggerConfiguration;}

            var msLoggingConfig = new MsLoggingConfig();
            loggingSection.Bind(msLoggingConfig);
            if (MsToSerilogLevelMap.TryGetValue(msLoggingConfig.LevelDefault, out var lDefault))
            {
                loggerConfiguration.MinimumLevel.Is(lDefault);
            }

            foreach (var (key, value) in msLoggingConfig.LogLevel.Where(x => x.Key != MsLoggingConfig.LevelDefaultKey))
            {
                if (MsToSerilogLevelMap.TryGetValue(value, out var lSource))
                {
                    loggerConfiguration.MinimumLevel.Override(key, lSource);
                }
            }

            return loggerConfiguration;
        }

        public class MsLoggingConfig
        {
            public static readonly string LevelDefaultKey = "Default";
            public static readonly string LevelSystemKey = "System";
            public static readonly string LevelMicrosoftKey = "Microsoft";

            public IDictionary<string, LogLevel> LogLevel { get; set; } = new Dictionary<string, LogLevel>();

            public LogLevel LevelDefault => this.LogLevel.TryGetValue(LevelDefaultKey, out var l)
                ? l
                : Microsoft.Extensions.Logging.LogLevel.Trace;

            public LogLevel LevelSystem => this.LogLevel.TryGetValue(LevelSystemKey, out var l) 
                ? l 
                : LevelDefault;

            public LogLevel LevelMicrosoft => this.LogLevel.TryGetValue(LevelMicrosoftKey, out var l)
                ? l
                : LevelDefault;
        }

        public static class ApplicationName
        {
            private const string Prefix = "shome.";
            private const string UnknownName = "unknown";
            static ApplicationName()
            {
                var name =  Assembly.GetEntryAssembly()?.GetName().Name;
                if (name?.StartsWith(Prefix) == true)
                {
                    Get = name.Substring(Prefix.Length);
                }
                else
                {
                    Get = name ?? UnknownName;
                }
            }
#nullable enable
            public static string Get { get; }
        }
    }
}
