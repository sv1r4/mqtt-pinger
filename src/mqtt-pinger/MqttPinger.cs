﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt_pinger.config;
using mqtt_pinger.persistence;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Protocol;

namespace mqtt_pinger
{
    public class MqttPinger:IHostedService
    {
        private readonly PingConfig _config;
        private readonly IManagedMqttClient _mqtt;
        private readonly ILogger _logger;
        private readonly ITargetStatePersistenceProvider _persistence;
        private readonly MqttConfig _mqttConfig;
        private readonly CancellationTokenSource _cancellationPingLoop = new CancellationTokenSource();

        public MqttPinger(ILogger<MqttPinger> logger, IOptions<PingConfig> config, IManagedMqttClient mqtt, ITargetStatePersistenceProvider persistence, IOptions<MqttConfig> mqttConfig)
        {
            _mqtt = mqtt;
            _persistence = persistence;
            _mqttConfig = mqttConfig.Value;
            _logger = logger;
            _config = config.Value;

            _mqtt.ConnectedHandler =new MqttClientConnectedHandlerDelegate(args =>
            {
                _logger.LogInformation("mqtt connected"); 
            });

        }

        public async Task PingAllForever(CancellationToken token)
        {
            try
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();

                    await Task.WhenAll(_config.Targets
                        .Select(t => PingOneTarget(t, token))
                        .ToArray());

                    await Task.Delay(_config.Interval, token);
                }
            }
            catch (OperationCanceledException)
            {
                _logger.LogWarning("Ping loop cancelled");
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private async Task PingOneTarget(Target target, CancellationToken token)
        {
            var isOkPing = await GetMultiPingResult(token, target);

            var prevPingState = await _persistence.GetStateAsync(target.Ip);

            if (isOkPing != prevPingState)
            {
                _logger.LogInformation("ping '{targetIp}' state changed. publish to '{targetTopic}'", target.Ip, target.Topic);
                await _persistence.StoreStateAsync(target.Ip, isOkPing);
                await _mqtt.PublishAsync(target.Topic, isOkPing ? "1" : "0",
                    MqttQualityOfServiceLevel.AtLeastOnce, true);
                await _mqtt.PublishAsync($"{target.Topic}/{_config.TopicChangePostfix.TrimStart('/')}",
                    isOkPing ? "1" : "0",
                    MqttQualityOfServiceLevel.AtLeastOnce, false);
            }
        }

        private async Task<bool> GetMultiPingResult(CancellationToken token, Target target)
        {
            for (var i = 1; i <= _config.PingConfirmationCount; i++)
            {
                try
                {
                    using var ping = new Ping();

                    var pingResult =
                        await ping.SendPingAsync(target.Ip, (int) _config.PingTimeout.TotalMilliseconds);


                    _logger.LogDebug(
                        "ping #{i} '{targetIp}' result '{pingResult}'", i, target.Ip, pingResult.Status.ToString());
                    if (pingResult.Status == IPStatus.Success)
                    {
                        _logger.LogInformation("ping {success} '{targetIp}' with {attempts} attempt", true, target.Ip, i);
                        return true;
                    }
                }
                catch (PingException ex)
                {
                    _logger.LogWarning(ex, "ping error");
                }

                await Task.Delay(_config.PingConfirmationInterval, token);
            }

            _logger.LogInformation("ping {success} '{targetIp}' with {attempts} attempts",false, target.Ip, _config.PingConfirmationCount);
            return false;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var optionBuilder = new MqttClientOptionsBuilder()
                .WithClientId($"mqtt-pinger-{Guid.NewGuid()}")
                .WithTcpServer(_mqttConfig.Host, _mqttConfig.Port);

            if (!string.IsNullOrWhiteSpace(_mqttConfig.User))
            {
                optionBuilder = optionBuilder.WithCredentials(_mqttConfig.User, _mqttConfig.Password);
            }

            var options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(optionBuilder.Build())
                .Build();
            
            await _mqtt.StartAsync(options);
            
#pragma warning disable 4014
            PingAllForever(_cancellationPingLoop.Token);
#pragma warning restore 4014
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                await _mqtt.StopAsync();
            }
            finally
            {
                _cancellationPingLoop.Cancel();
            }
        }
    }
}
