﻿using System.Net;

namespace mqtt_pinger.config
{
    public class Target
    {
        public string Ip { get; set; }
        public string Topic { get; set; }
    }
}
