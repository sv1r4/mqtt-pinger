﻿using System;
using System.Collections.Generic;

namespace mqtt_pinger.config
{
    public class PingConfig
    {
        public TimeSpan Interval { get; set; } 
        public TimeSpan PingTimeout { get; set; } 
        public IEnumerable<Target> Targets { get; set; }
        public string TopicChangePostfix { get; set; }
        public int PingConfirmationCount { get; set; }
        public TimeSpan PingConfirmationInterval { get; set; }
    }
}
